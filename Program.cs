using System.Drawing;
using System.Drawing.Imaging;

namespace Assignment1
{
    abstract class ImageBinarization : IDisposable
    {
        private Bitmap _bitmap;
        private bool _disposed ;

        public ImageBinarization(Bitmap bitmap)
        {
            this._bitmap = bitmap;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _bitmap.Dispose();
                }

                _disposed = true;
            }
        }
        ~ImageBinarization()
        {
            Dispose(false);
        }

        static void Main(string[] args)
        {
            string directory = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string imagePath = Path.Combine(directory, "grayimage.png");
          
            Bitmap originalImage = new Bitmap(imagePath);
            Bitmap image = ConvertToGrayscale(originalImage);

            Bitmap staticThresholdImage = ConvertToBlackAndWhite(image);

            Bitmap meanThresholdImage = ConvertToBlackAndWhite(image, 0, true);

            staticThresholdImage.Save("Static.bmp", ImageFormat.Bmp);
            meanThresholdImage.Save("Mean.bmp", ImageFormat.Bmp);

            Console.WriteLine("Image converted to binary using static and mean threshold successfully.");
            
            originalImage.Dispose();
            image.Dispose();
            staticThresholdImage.Dispose();
            meanThresholdImage.Dispose();

        }

        static Bitmap ConvertToGrayscale(Bitmap image)
        {
            if (image.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                return image;
            }

            int width = image.Width;
            int height = image.Height;

            Bitmap convertedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = convertedImage.Palette;
            for (int i = 0; i < 256; i++)
            {
                palette.Entries[i] = Color.FromArgb(i, i, i);
            }

            convertedImage.Palette = palette;

            BitmapData convertedImageData = convertedImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            BitmapData imageData = image.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            int originalOffset = imageData.Stride - width * 3;
            int convertedOffset = convertedImageData.Stride - width;

            unsafe
            {
                byte* originalPointer = (byte*)imageData.Scan0;
                byte* convertedImagePointer = (byte*)convertedImageData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        byte r = originalPointer[2];
                        byte g = originalPointer[1];
                        byte b = originalPointer[0];

                        byte gray = (byte)(0.3 * r + 0.59 * g + 0.11 * b);
                        convertedImagePointer[0] = gray;

                        originalPointer += 3;
                        convertedImagePointer += 1;
                    }

                    originalPointer += originalOffset;
                    convertedImagePointer += convertedOffset;
                }
            }

            image.UnlockBits(imageData);
            convertedImage.UnlockBits(convertedImageData);
            return convertedImage;
        }

        static Bitmap ConvertToBlackAndWhite(Bitmap grayscaleImage, int threshold =0, bool useMeanThreshold=false)
        {
            int width = grayscaleImage.Width;
            int height = grayscaleImage.Height;

            Bitmap blackAndWhiteImage = new Bitmap(width, height, PixelFormat.Format1bppIndexed);

            BitmapData grayscaleData = grayscaleImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            BitmapData blackAndWhiteData = blackAndWhiteImage.LockBits(
                new Rectangle(0, 0, blackAndWhiteImage.Width, blackAndWhiteImage.Height),
                ImageLockMode.WriteOnly, PixelFormat.Format1bppIndexed);

            int grayStride = grayscaleData.Stride;
            IntPtr grayScan0 = grayscaleData.Scan0;

            int binaryStride = blackAndWhiteData.Stride;
            IntPtr binaryScan0 = blackAndWhiteData.Scan0;

            if (useMeanThreshold)
            {
                CalculateMean(ref grayscaleData, ref threshold);
            }
            else
            {
                threshold=155;
            }

            unsafe
            {
                for (int y = 0; y < height; y++)
                {
                    byte* grayPointer = (byte*)grayScan0 + y * grayStride;
                    byte* binaryPointer = (byte*)binaryScan0 + y * binaryStride;

                    for (int x = 0; x < width; x++)
                    {
                        if (*grayPointer > threshold)
                        {
                            int bitIndex = x % 8;
                            byte mask = (byte)(0x80 >> bitIndex);
                            binaryPointer[x / 8] |= mask;
                        }
                        else
                        {
                            int bitIndex = x % 8;
                            byte mask = (byte)(0x80 >> bitIndex);
                            binaryPointer[x / 8] &= (byte)~mask;
                        }

                        grayPointer++;
                    }
                }
            }

            grayscaleImage.UnlockBits(grayscaleData);
            blackAndWhiteImage.UnlockBits(blackAndWhiteData);

            return blackAndWhiteImage;
        }

        static void CalculateMean(ref BitmapData grayscaleData, ref int threshold)
        {
            unsafe
            {
                byte* scan0 = (byte*)grayscaleData.Scan0;
                int stride = grayscaleData.Stride;

                int width = grayscaleData.Width;
                int height = grayscaleData.Height;

                int sum = 0;
                int count = 0;

                for (int y = 0; y < height; y++)
                {
                    byte* pointer = scan0 + y * stride;
                    for (int x = 0; x < width; x++)
                    {
                        byte gray = pointer[x];
                        sum += gray;
                        count++;
                        pointer++;
                    }
                }

                threshold = sum / count;
                Console.WriteLine(threshold);
            }
        }
    }
}
